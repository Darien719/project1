**Reimbursement App**

Project Description
The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.


**Technologies Used**

Java

Javalin

Mockito

JUnit

Selenium

SQL

HTML

CSS/ BootStrap

JavaScript

AJAX


**Features**

**Users** are able to create accounts.

**Users** are able to login.

**Employees** are able to create reimbursements. 

**Employees** are able to view their past reimbursements.

**Managers** are able to create reimbursement.

**Managers** are able to view their past reimbursements.

**Managers** are view all reimbursements. 

**Managers** are able to either accept or deny reimbursement that are not their own.


**To-do list:**

Apply new technologies that I have learned since making this project, such as Angular and BootStrap.

**Getting Started**

**Should be able to still function as I have not closed the SQL server I made for the project**
1) git clone https://gitlab.com/Darien719/project1.git
2) Set up the project in an IDE like Eclipse that'll work with Maven project and download the 
3) mvn install
4) Run application from the main method, Javalin will provide a localhost of the app.



