package project1.com.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project1.dao.DBCConnection;
import com.project1.dao.UserDao;
import com.project1.models.User;
import com.project1.service.UserService;

public class UserServiceTest {


    @Mock
    private DBCConnection dbc;

    @Mock 
    private Connection c;

    @Mock 
    private PreparedStatement ps;

    @Mock 
    private CallableStatement cs;

    @Mock
    private ResultSet rs;

    @Mock
    private UserDao udt;

    private User testUser;

    @BeforeClass
    public static void setUpBeforeClass()  throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(dbc.getDBConnection()).thenReturn(c);
        when(c.prepareCall(any(String.class))).thenReturn(cs);
        testUser = new User(1,"UserName","Password","Darien","Sosa","Email@email.com",1);
        when(udt.createUser(testUser)).thenReturn(1);
        when(udt.validateUser(testUser)).thenReturn(testUser);

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testCreateUser() {
        assertEquals(new UserService(udt).createUser(testUser),1);
    }

    @Test
    public void validateUser() {
        assertEquals(new UserService(udt).verifyUser(testUser).getUserId(), testUser.getUserId());
    }

}
