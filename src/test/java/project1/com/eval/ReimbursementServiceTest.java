package project1.com.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.project1.dao.DBCConnection;
import com.project1.dao.ReimbursementDao;
import com.project1.models.Reimbursement;
import com.project1.models.User;
import com.project1.service.ReimbursementService;


public class ReimbursementServiceTest {
	@Mock
	private DBCConnection dbc;
	
	@Mock 
	private Connection c;
	
	@Mock 
	private PreparedStatement ps;
	
	@Mock 
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	@Mock
	private ReimbursementDao rd;
	
	private User testUser;
	
	private Reimbursement testReimbursement;
	List<Reimbursement> reimList = new ArrayList<>();
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dbc.getDBConnection()).thenReturn(c);
		when(c.prepareCall(any(String.class))).thenReturn(cs);
		testUser = new User(1,"UserName","Password","Darien","Sosa","Email@email.com",1);
		testReimbursement = new Reimbursement(1, 100, "Description", 1, 1, 1, 1);
		reimList.add(testReimbursement);
		when(rd.createReimbursement(testReimbursement)).thenReturn(1);
		when(rd.getReimbursements()).thenReturn(reimList);
		when(rd.getReimbursementsByAuthor(testUser)).thenReturn(reimList);
		when(rd.getReimbursementByStatus(testReimbursement.getStatusID())).thenReturn(reimList);
		when(rd.getReimbursementsById(testReimbursement.getReimbId())).thenReturn(testReimbursement);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testCreateReimbursement() {
		assertEquals(new ReimbursementService(rd).createReimbursement(testReimbursement),1);
	}
	
	@Test
	public void testGetReimbursement() {
		assertEquals(new ReimbursementService(rd).getAllReimbursements().size(),1);
	}
	
	@Test
	public void testGetReimbursementByAuthor() {
		assertEquals(new ReimbursementService(rd).getReimbursementsByAuthor(testUser).get(0).getAuthorID(),testUser.getUserId());
	}
	
	@Test
	public void testGetReimbursementByStatus() {
		assertEquals(new ReimbursementService(rd).getReimbursementsByStatus(testReimbursement.getStatusID()).get(0).getStatusID(),testReimbursement.getStatusID());
	}
	
	@Test
	public void testGetReimbursementById() {
		
		assertEquals(new ReimbursementService(rd).getReimbursementsById(testReimbursement.getReimbId()).getReimbId(),testReimbursement.getReimbId());
	}
	
	
	
}
