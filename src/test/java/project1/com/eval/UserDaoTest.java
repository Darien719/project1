package project1.com.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import com.project1.dao.DBCConnection;
import com.project1.dao.UserDao;
import com.project1.models.User;

public class UserDaoTest {
	private String url = "jdbc:mariadb://revy-sempai.cym6nvtcsn48.us-east-2.rds.amazonaws.com:3306/project1_ers";
	private String username = "ersuser";
	private String password = "password";
	
	@Mock
	private DBCConnection dbc;
	
	@Mock 
	private Connection c;
	
	@Mock 
	private PreparedStatement ps;
	
	@Mock 
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	private User testUser;
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dbc.getDBConnection()).thenReturn(c);
		when(c.prepareCall(any(String.class))).thenReturn(cs);
		testUser = new User(1,"UserName","Password","Darien","Sosa","Email@email.com",1);
		//when(cs.setString(parameterIndex, x);)
		when(cs.executeUpdate()).thenReturn(1);
		when(cs.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser.getUserId());
		when(rs.getString(2)).thenReturn(testUser.getUsername());
		when(rs.getString(3)).thenReturn(testUser.getPassword());
		when(rs.getString(4)).thenReturn(testUser.getFirstName());
		when(rs.getString(5)).thenReturn(testUser.getLastName());
		when(rs.getString(6)).thenReturn(testUser.getEmail());
		when(rs.getInt(7)).thenReturn(testUser.getUserRoleId());
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testCreateUser() {
		assertEquals(new UserDao(dbc).createUser(testUser), 1);
	}
	
	@Test
	public void testValidateUser() {
		
		assertEquals(new UserDao(dbc).validateUser(testUser).getPassword(), testUser.getPassword());
		assertEquals(new UserDao(dbc).validateUser(testUser).getUsername(), testUser.getUsername());
	}
	
	
}
