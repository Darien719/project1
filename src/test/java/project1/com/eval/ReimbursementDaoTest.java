package project1.com.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project1.dao.DBCConnection;
import com.project1.dao.ReimbursementDao;
import com.project1.models.Reimbursement;
import com.project1.models.User;

public class ReimbursementDaoTest {
	private String url = "jdbc:mariadb://revy-sempai.cym6nvtcsn48.us-east-2.rds.amazonaws.com:3306/project1_ers";
	private String username = "ersuser";
	private String password = "password";

	@Mock
	private DBCConnection dbc;

	@Mock
	private Connection c;

	@Mock
	private PreparedStatement ps;

	@Mock
	private CallableStatement cs;

	@Mock
	private ResultSet rs;

	Reimbursement testReimbursement;
	List<Reimbursement> reimList = new ArrayList<>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dbc.getDBConnection()).thenReturn(c);
		when(c.prepareCall(any(String.class))).thenReturn(cs);
		testReimbursement = new Reimbursement(1, 100, "Description", 1, 1, 1, 1);
		reimList.add(testReimbursement);
		when(cs.executeUpdate()).thenReturn(1);
		when(cs.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(1);
		when(rs.getDouble(2)).thenReturn(100.00);
		when(rs.getTimestamp(3)).thenReturn(null);
		when(rs.getTimestamp(4)).thenReturn(null);
		when(rs.getString(5)).thenReturn("Description");
		when(rs.getBlob(6)).thenReturn(null);
		when(rs.getInt(7)).thenReturn(1);
		when(rs.getInt(8)).thenReturn(1);
		when(rs.getInt(9)).thenReturn(1);
		when(rs.getInt(10)).thenReturn(1);

	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testcreateReimbursement() {
		assertEquals(new ReimbursementDao(dbc).createReimbursement(testReimbursement), 1);
	}

	@Test
	public void testUpdateReimb() {
		assertEquals(new ReimbursementDao(dbc).updateReimbursementStatus(testReimbursement), 1);
	}

	@Test
	public void testGetReimbursements() {

		assertEquals(new ReimbursementDao(dbc).getReimbursements().size(), 1);
	}

	@Test
	public void testGetReimbursementsByAuthor() {
		User user = new User(1);
		assertEquals(new ReimbursementDao(dbc).getReimbursementsByAuthor(user).get(0).getAuthorID(),user.getUserId());
	}
	
	@Test
	public void testGetReimbursementsByStatus() {
		
		assertEquals(new ReimbursementDao(dbc).getReimbursementByStatus(testReimbursement.getStatusID()).get(0).getStatusID(),testReimbursement.getStatusID());
	}

	@Test
	public void testGetReimbursementsById() {
		
		assertEquals(new ReimbursementDao(dbc).getReimbursementsById(testReimbursement.getReimbId()).getReimbId(),testReimbursement.getReimbId());
	}

}
