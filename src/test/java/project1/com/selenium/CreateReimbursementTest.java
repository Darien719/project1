package project1.com.selenium;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class CreateReimbursementTest {
	CreateReimbursement createPage;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		String filePath = "src/test/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception{
		this.createPage = new CreateReimbursement(driver);
		
	}
	
	@After
	public void tearDown() throws Exception{
		
	}
	
	@Test
	public void testSuccesfulCreateReimb(){
		createPage.setUsername("Durna");
		createPage.setPassword("password");
		createPage.loginSubmit();
		//Wait until we are logged in. Once we are logged in we go to the create page. 
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/index.html"));
		createPage.nagivateToCreate();
		wait.until(ExpectedConditions.urlMatches("/create-reimb.html"));
		createPage.initializeReimbursementElements();
		createPage.setAmount("100");
		createPage.setDescription("Description");
		createPage.reimSubmit();
		wait.until(ExpectedConditions.urlMatches("/view-reimbursements.html"));
		assertEquals("http://localhost:2078/html/view-reimbursements.html", driver.getCurrentUrl());
		
		
		
		
	}

}
