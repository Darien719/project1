package project1.com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AcceptReimbursement {

	private WebDriver driver; 
	private WebElement username;
	private WebElement password;
	private WebElement loginSubmit;

	
	public AcceptReimbursement(WebDriver driver) {
		this.driver = driver; 
		this.nagivateToLogIn();
		this.username = driver.findElement(By.id("username-text"));
		this.password = driver.findElement(By.id("password-text"));
		this.loginSubmit = driver.findElement(By.id("login-button"));
	}
	
	public void nagivateToLogIn() {
		this.driver.get("http://localhost:2078/html/sign-in.html");
	}
	
	public void nagivateToReview() {
		this.driver.get("http://localhost:2078/html/review-reimbursements.html");
	}
	
	
	public void loginSubmit() {
		this.loginSubmit.click();
	}
	
	public void setUsername(String name) {
		this.username.clear();
		this.username.sendKeys(name);
	}
	
	public String getUsername() {
		return this.username.getAttribute("value");
	}
	
	public void setPassword(String power) {
		this.password.clear();
		this.password.sendKeys(power);
	}
	
	public String getPassword() {
		return this.password.getAttribute("value");
	}
	
	public void initializeViewReimbursementElements() {
		

	}
	

	
}
