package com.example.controller;
import com.project1.models.User;
import com.project1.service.UserService;

import io.javalin.http.Handler;

public class UserController {
	
	private UserService uServ;
	
	public UserController() {
		super();
	}
	
	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}
	

	
	public Handler postUser = (ctx) ->{
		User user = new User(ctx.formParam("username"),ctx.formParam("password"),ctx.formParam("first_name"),ctx.formParam("last_name"),
				ctx.formParam("email"),1);
		
		int result = uServ.createUser(user);
		if (result == 1) {
			ctx.redirect("/html/sign-in.html");
		}else {
			ctx.redirect("/html/sign-up.html");
		}
	}; 
	
	public Handler logInUser = (ctx) ->{
		User user = new User(ctx.formParam("username"),ctx.formParam("password"));
		
		
		User userValidated= uServ.verifyUser(user);
		
		
		
		if(userValidated != null) {
			ctx.redirect("/html/index.html");
			ctx.sessionAttribute("user", userValidated);
		}
		else {
			ctx.redirect("/html/sign-in.html");
		}
		
	};
	
	public Handler logOutUser = (ctx) ->{
		
			ctx.sessionAttribute("user", "");
			ctx.redirect("/html/sign-in.html");

		
	};
	
	public Handler getSessUser = (ctx) ->{
		
		User user = (User)ctx.sessionAttribute("user");
		
		ctx.json(user);
	};
	
	
}
