package com.example.controller;

import java.util.List;

import com.project1.models.Reimbursement;
import com.project1.models.User;
import com.project1.service.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {
	private ReimbursementService rServ;

	public ReimbursementController() {
		super();
	}

	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ = rServ;
	}

	public Handler postReimbursement = (ctx) -> {
		Reimbursement reim = new Reimbursement(Double.parseDouble(ctx.formParam("amount")),
				ctx.formParam("description"), Integer.parseInt(ctx.formParam("author_id")),
				Integer.parseInt(ctx.formParam("type_id")));

		int result = rServ.createReimbursement(reim);
		if (result == 1) {

			ctx.redirect("/html/view-reimbursements.html");
		} else {

			// Not created
		}
	};

	public Handler getReimbursements = (ctx) -> {

		List<Reimbursement> reimList = rServ.getAllReimbursements();
		ctx.json(reimList);

	};

	public Handler getReimbursementsByAuthor = (ctx) -> {

		User user = (User) ctx.sessionAttribute("user");

		List<Reimbursement> reimList = rServ.getReimbursementsByAuthor(user);
		ctx.json(reimList);

	};

	public Handler denyReimbursement = (ctx) -> {

		User user = (User) ctx.sessionAttribute("user");
		Reimbursement reimb = new Reimbursement(Integer.parseInt(ctx.pathParam("reimb_id")), user.getUserId(), 3);
		rServ.updateReimbursementStatus(reimb);
		ctx.redirect("/html/review-reimbursements.html");

	};

	public Handler approveReimbursement = (ctx) -> {

		User user = (User) ctx.sessionAttribute("user");
		Reimbursement reimb = new Reimbursement(Integer.parseInt(ctx.pathParam("reimb_id")), user.getUserId(), 2);
		rServ.updateReimbursementStatus(reimb);
		ctx.redirect("/html/review-reimbursements.html");

	};

	public Handler getReimbursementsByStatus = (ctx) -> {

		List<Reimbursement> reimList = rServ.getReimbursementsByStatus(Integer.parseInt(ctx.pathParam("status_id")));
		ctx.json(reimList);

	};

	public Handler getReimbursementsById = (ctx) -> {

		Reimbursement reim = rServ.getReimbursementsById(Integer.parseInt(ctx.pathParam("reimb_id")));
		if(reim == null) {
			ctx.status(404);
		}else {
		ctx.json(reim);
		}
	};

}
