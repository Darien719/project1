package com.project1.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.project1.models.User;

public class UserDao {

	private DBCConnection dbc;

	public UserDao() {
	}

	public UserDao(DBCConnection dbc) {
		super();
		this.dbc = dbc;
	}

	/*
	 * Creates a new user entity in the database using the user object, Returns how
	 * many rows were effected. Should return 1 if it was able to create 1 user.
	 */
	public int createUser(User user) {

		try (Connection con = dbc.getDBConnection()) {

			String sql = "{call create_users(?,?,?,?,?,?) }";
			CallableStatement cs = con.prepareCall(sql);

			// Need to make sure we're not inserting empty values because JDBC is not
			// sending "" as null.
			if (user.getUsername() == "" || user.getPassword() == "" || user.getEmail() == "") {
				return -1;
			}
			;

			cs.setString(1, user.getUsername());
			cs.setString(2, user.getPassword());
			cs.setString(3, user.getFirstName());
			cs.setString(4, user.getLastName());
			cs.setString(5, user.getEmail());
			cs.setInt(6, user.getUserRoleId());

			int status = cs.executeUpdate();
			return status;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;

	}

	public User validateUser(User user) {
		try (Connection con = dbc.getDBConnection()) {
			
			
			String sql = "{call get_user_by_credentials(?,?) }";
			CallableStatement cs = con.prepareCall(sql);
			
			cs.setString(1, user.getUsername());
			cs.setString(2, user.getPassword());
			
			
			ResultSet rs = cs.executeQuery();
			
			// Get the first one, if I can't then it's not there and return null
						if (rs.next() == false) {
							return null;
						}

						return new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;

}

}
