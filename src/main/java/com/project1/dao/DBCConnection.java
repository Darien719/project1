package com.project1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBCConnection {
	
	private String url = "jdbc:mariadb://revy-sempai.cym6nvtcsn48.us-east-2.rds.amazonaws.com:3306/project1_ers";
	private String username = "ersuser";
	private String password = "password";

	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}
	
}
