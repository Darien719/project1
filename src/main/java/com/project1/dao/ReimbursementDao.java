package com.project1.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.project1.models.Reimbursement;
import com.project1.models.User;

public class ReimbursementDao {

	private DBCConnection dbc;

	public ReimbursementDao() {
		super();
	}

	public ReimbursementDao(DBCConnection dbc) {
		super();
		this.dbc = dbc;
	}

	/*
	 * Creates a new reimbursement entity in the database using the reimbursement
	 * object, Returns how many rows were effected. Should return 1 if it was able
	 * to create 1 user.
	 */
	public int createReimbursement(Reimbursement reimb) {

		try (Connection con = dbc.getDBConnection()) {

			String sql = "{call create_reimbursements(?,?,null,?,?) }";
			CallableStatement cs = con.prepareCall(sql);

			cs.setDouble(1, reimb.getAmount());
			cs.setString(2, reimb.getDescription());
			cs.setInt(3, reimb.getAuthorID());
			cs.setInt(4, reimb.getTypeID());

			int status = cs.executeUpdate();
			return status;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;

	}
	
	public int updateReimbursementStatus(Reimbursement reimb) {
		try (Connection con = dbc.getDBConnection()) {

			String sql = "{call update_reimbursements_status(?,?,?) }";
			CallableStatement cs = con.prepareCall(sql);

			cs.setInt(1, reimb.getReimbId());
			cs.setInt(2, reimb.getResolverID());
			cs.setInt(3, reimb.getStatusID());
		
		   return cs.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public List<Reimbursement> getReimbursements() {

		List<Reimbursement> reimList = new ArrayList<>();

		try (Connection con = dbc.getDBConnection()) {

			String sql = "{call select_all_reimbursements() }";
			CallableStatement cs = con.prepareCall(sql);

			ResultSet rs = cs.executeQuery();

			while (rs.next()) {
				reimList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimList;

	}

	public List<Reimbursement> getReimbursementsByAuthor(User user) {

		List<Reimbursement> reimList = new ArrayList<>();

		try (Connection con = dbc.getDBConnection()) {

			String sql = "{call select_reimbursements_by_author(?) }";
			CallableStatement cs = con.prepareCall(sql);

			cs.setInt(1, user.getUserId());

			ResultSet rs = cs.executeQuery();

			while (rs.next()) {
				reimList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimList;

	}
	
	public List<Reimbursement> getReimbursementByStatus(int status) {

		List<Reimbursement> reimList = new ArrayList<>();

		try (Connection con = dbc.getDBConnection()) {

			String sql = "{call get_reimbursement_by_status(?) }";
			CallableStatement cs = con.prepareCall(sql);
			
			cs.setInt(1, status);
			ResultSet rs = cs.executeQuery();
			
			
			while (rs.next()) {
				reimList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	
		return reimList;

	}
	
	public Reimbursement getReimbursementsById(int id) {

		Reimbursement reim;

		try (Connection con = dbc.getDBConnection()) {

			String sql = "{call select_reimbursements_by_id(?) }";
			CallableStatement cs = con.prepareCall(sql);

			cs.setInt(1, id);

			ResultSet rs = cs.executeQuery();

			while (rs.next()) {
				return reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}


	

}
