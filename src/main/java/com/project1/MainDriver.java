package com.project1;

import com.example.controller.ReimbursementController;
import com.example.controller.UserController;
import com.project1.dao.DBCConnection;
import com.project1.dao.ReimbursementDao;
import com.project1.dao.UserDao;
import com.project1.service.ReimbursementService;
import com.project1.service.UserService;

import io.javalin.Javalin;

public class MainDriver {
	public static void main(String[] args) {
		
		UserController uCon = new UserController(new UserService(new UserDao(new DBCConnection())));
		ReimbursementController rCon = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new DBCConnection())));
		
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/frontend");
			config.addStaticFiles("/frontend/images");

		});
		
		app.start(2078);
		
		app.post("/users/create", uCon.postUser);
		app.post("/users/login", uCon.logInUser);
		app.get("/users/getusersession", uCon.getSessUser);
		app.post("/users/logout", uCon.logOutUser);
		app.post("/reimbursements/create", rCon.postReimbursement);
		app.get("/reimbursements/getall", rCon.getReimbursements);
		app.get("/reimbursements/getbyuser", rCon.getReimbursementsByAuthor);
		app.post("/reimbursements/approve/:reimb_id", rCon.approveReimbursement);
		app.post("/reimbursements/deny/:reimb_id", rCon.denyReimbursement);
		app.get("/reimbursements/getbystatus/:status_id", rCon.getReimbursementsByStatus);
		app.get("/reimbursements/get/:reimb_id", rCon.getReimbursementsById);
		
		
	}
}
