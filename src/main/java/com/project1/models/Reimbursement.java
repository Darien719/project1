package com.project1.models;

import java.sql.Blob;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Reimbursement {
	private int reimbId;
	private double amount;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	private  Timestamp timeSubmitted;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	private Timestamp timeResolved; 
	private String description;
	private Blob receipt;
	private int authorID;
	private int resolverID;
	private int statusID;
	private int typeID;
	
	public Reimbursement() {
		super();
	}
	
	public Reimbursement(double amount, String description, int authorID, int typeID ) {
		super();
		this.amount = amount;
		this.description = description;
		this.authorID = authorID;
		this.typeID = typeID;
		
	}
	
	
	public Reimbursement(int reimbId, double amount, String description, int authorID, int resolverID, int statusID,
			int typeID) {
		super();
		this.reimbId = reimbId;
		this.amount = amount;
		this.description = description;
		this.authorID = authorID;
		this.resolverID = resolverID;
		this.statusID = statusID;
		this.typeID = typeID;
	}

	public Reimbursement(int reimbId, int resolverID, int statusID) {
		super();
		this.reimbId = reimbId;
		this.resolverID = resolverID;
		this.statusID = statusID;
	}

	public Reimbursement(int reimbId, double amount, Timestamp timeSubmitted, Timestamp timeResolved,
			String description, Blob receipt, int authorID, int resolverID, int statusID, int typeID) {
		super();
		this.reimbId = reimbId;
		this.amount = amount;
		this.timeSubmitted = timeSubmitted;
		this.timeResolved = timeResolved;
		this.description = description;
		this.receipt = receipt;
		this.authorID = authorID;
		this.resolverID = resolverID;
		this.statusID = statusID;
		this.typeID = typeID;
	}

	
	
	public Reimbursement(int reimbId, double amount, Timestamp timeSubmitted, Timestamp timeResolved,
			String description, int authorID, int resolverID, int statusID, int typeID) {
		super();
		this.reimbId = reimbId;
		this.amount = amount;
		this.timeSubmitted = timeSubmitted;
		this.timeResolved = timeResolved;
		this.description = description;
		this.authorID = authorID;
		this.resolverID = resolverID;
		this.statusID = statusID;
		this.typeID = typeID;
	}

	
	
	public Reimbursement(int reimbId, double amount, Timestamp timeSubmitted, String description, Blob receipt,
			int authorID, int statusID, int typeID) {
		super();
		this.reimbId = reimbId;
		this.amount = amount;
		this.timeSubmitted = timeSubmitted;
		this.description = description;
		this.receipt = receipt;
		this.authorID = authorID;
		this.statusID = statusID;
		this.typeID = typeID;
	}

	public int getReimbId() {
		return reimbId;
	}
	public void setReimbId(int reimbId) {
		this.reimbId = reimbId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Timestamp getTimeSubmitted() {
		return timeSubmitted;
	}
	public void setTimeSubmitted(Timestamp timeSubmitted) {
		this.timeSubmitted = timeSubmitted;
	}
	public Timestamp getTimeResolved() {
		return timeResolved;
	}
	public void setTimeResolved(Timestamp timeResolved) {
		this.timeResolved = timeResolved;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Blob getReceipt() {
		return receipt;
	}
	public void setReceipt(Blob receipt) {
		this.receipt = receipt;
	}
	public int getResolverID() {
		return resolverID;
	}
	public void setResolverID(int resolverID) {
		this.resolverID = resolverID;
	}
	public int getStatusID() {
		return statusID;
	}
	public void setStatusID(int statusID) {
		this.statusID = statusID;
	}
	public int getTypeID() {
		return typeID;
	}
	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}
	public int getAuthorID() {
		return authorID;
	}
	@Override
	public String toString() {
		return "Reimbursement [reimbId=" + reimbId + ", amount=" + amount + ", timeSubmitted=" + timeSubmitted
				+ ", timeResolved=" + timeResolved + ", description=" + description + ", receipt=" + receipt
				+ ", authorID=" + authorID + ", resolverID=" + resolverID + ", statusID=" + statusID + ", typeID="
				+ typeID + "]";
	}
	
	
	
	
}
