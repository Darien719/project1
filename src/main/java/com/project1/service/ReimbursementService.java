package com.project1.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


import com.project1.dao.ReimbursementDao;
import com.project1.models.Reimbursement;
import com.project1.models.User;

@SuppressWarnings("unused")
public class ReimbursementService {

	private ReimbursementDao rDao;
	public final static Logger log = Logger.getLogger(ReimbursementService.class);

	public ReimbursementService() {
		super();
	}

	public ReimbursementService(ReimbursementDao rDao) {
		super();
		this.rDao = rDao;
	}

	public int createReimbursement(Reimbursement reimb) {
		int result = rDao.createReimbursement(reimb);
		return result;
	}

	public List<Reimbursement> getAllReimbursements() {

		List<Reimbursement> reimList = rDao.getReimbursements();
		return reimList;

	}

	public List<Reimbursement> getReimbursementsByAuthor(User user) {

		List<Reimbursement> reimList = rDao.getReimbursementsByAuthor(user);
		return reimList;

	}

	public void updateReimbursementStatus(Reimbursement reimb) {
		rDao.updateReimbursementStatus(reimb);
	}
	
	public List<Reimbursement> getReimbursementsByStatus(int status) {

		List<Reimbursement> reimList = rDao.getReimbursementByStatus(status);
		return reimList;

	}
	
	public Reimbursement getReimbursementsById(int id) {

		Reimbursement reim = rDao.getReimbursementsById(id);
		return reim;

	}

}
