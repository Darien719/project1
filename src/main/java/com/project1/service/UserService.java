package com.project1.service;

import org.apache.log4j.Logger;

import com.project1.dao.UserDao;
import com.project1.models.User;

public class UserService {
	
	private UserDao uDao;
	public final static Logger log = Logger.getLogger(UserService.class);
	
	public UserService() {
		super();
	}
	
	public UserService(UserDao uDao) {
		super();
		this.uDao = uDao;
	}
	
	public int createUser(User user) {
		int result = uDao.createUser(user);
		return result;
	}
	
	public User verifyUser(User user) {
		User userValidated = uDao.validateUser(user);
		if(userValidated != null) {
		
			return userValidated;
		}

		return null;
	}
	
}
