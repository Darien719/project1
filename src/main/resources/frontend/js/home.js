/**
 * Check to see if user is logged in or not
 */
window.onload = function() {

	getSessionUser();


}

function getSessionUser() {
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let user = JSON.parse(xhttp.responseText);
			loggedInDomManip(user);
		}

	}

	xhttp.open("GET", "http://localhost:2078/users/getusersession");

	xhttp.send();


}



function loggedInDomManip(userJSON) {
	if (userJSON.userRoleId == 2) {
	var navdivider = document.getElementById("reimb-dropdown");
	navdivider.innerHTML += `<li><a class="dropdown-item" href="/html/review-reimbursements.html" id="review-button">Review
	Reimbursements</a></li>`;
	}

	document.getElementById("sign-up-button").remove();
	document.getElementById("sign-in-button").innerText = "Log Out";
	document.getElementById("sign-in-button").addEventListener("click", logOut);
}


function logOut() {
	let xhttp = new XMLHttpRequest();


	xhttp.open("POST", "http://localhost:2078/users/logout");

	xhttp.send();
}
