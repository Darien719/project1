/**
 * Check to see if user is logged in or not
 */
window.onload = function () {


	getSessionUser();
	getReimbursements();

}

function getSessionUser() {
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function () {

		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let user = JSON.parse(xhttp.responseText);

			loggedInDomManip(user);

		} else if (xhttp.status == 500) {
			location.href = "/html/index.html";
		}

	}

	xhttp.open("GET", "http://localhost:2078/users/getusersession");

	xhttp.send();


}

function loggedInDomManip(userJSON) {

	if (userJSON.userRoleId == 1) {
		document.getElementById("review-button").remove();
	}

	document.getElementById("sign-up-button").remove();
	document.getElementById("sign-in-button").innerText = "Log Out";
	document.getElementById("sign-in-button").addEventListener("click", logOut);

}

function getReimbursements() {
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function () {

		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let reimbursements = JSON.parse(xhttp.responseText);

			reimbursementsDomManip(reimbursements);


		}

	}

	xhttp.open("GET", "http://localhost:2078/reimbursements/getbyuser");

	xhttp.send();

}

function reimbursementsDomManip(reimbList) {

	var table = document.getElementById("myTable");

	for (var i = 0; i < reimbList.length; i++) {
		var obj = reimbList[i];
		var row = table.insertRow(1);
		var id = row.insertCell(0);
		var amount = row.insertCell(1);
		var author = row.insertCell(2);
		var desc = row.insertCell(3);
		var submitted = row.insertCell(4);
		var resolved = row.insertCell(5);
		var resolver = row.insertCell(6);
		var status = row.insertCell(7);
		var type = row.insertCell(8);
	


		id.innerHTML = obj.reimbId;
		amount.innerHTML = "$" + obj.amount;
		author.innerHTML = obj.authorID;
		desc.innerHTML = obj.description;
		submitted.innerHTML = obj.timeSubmitted;
		resolved.innerHTML = obj.timeResolved;
		resolver.innerHTML = obj.resolverID;
		switch (obj.statusID) {
			case 1:
				status.innerHTML = "Pending";
				break;
			case 2:
				status.innerHTML = "Approved";
				break;
			case 3:
				status.innerHTML = "Denied";
				break;

		}

		switch (obj.typeID) {
			case 1:
				type.innerHTML = "LODGING";
				break;
			case 2:
				type.innerHTML = "TRAVEL";
				break;
			case 3:
				type.innerHTML = "FOOD";
				break;
			case 4:
				type.innerHTML = "OTHER";
				break;

		}



	}




}


function logOut() {
	let xhttp = new XMLHttpRequest();


	xhttp.open("POST", "http://localhost:2078/users/logout");

	xhttp.send();
}
